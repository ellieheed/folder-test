import { Injectable } from '@angular/core';

import { files } from './files';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  files = files;

  constructor() { }

  getAllFiles(): any {
    return this.files;
  }

}
