import { Component, OnInit } from '@angular/core';
import { FilesService } from '../files.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {

  files = [];
  sortBy = 0;
  crumbs = "root";

  nameIcon;
  typeIcon;
  addedIcon;
  crumbsIcon = "./assets/icons/arrow_right.svg";
  homeIcon = "./assets/icons/home.svg";
  minusIcon = "./assets/icons/minus.svg";
  ascIcon = "./assets/icons/arrow_up.svg";
  descIcon = "./assets/icons/arrow_down.svg";

  constructor( private fileService: FilesService ) {

  }

  ngOnInit() {
    this.files = this.fileService.getAllFiles();

    this.resetIcons();
    this.sortByName();

  }

  sortByName() {

    this.resetIcons();

    if(this.sortBy == 0) {

      this.nameIcon = this.ascIcon;

      // Sort the files alphabetically ascending
      this.files.sort((a, b) => {
        return this.compareStrings(a.name, b.name);
      });

      this.sortBy = 1;
    } else {

      this.nameIcon = this.descIcon;

      // Sort the files alphabetically descending
      this.files.sort((a, b) => {
        return this.compareStrings(b.name, a.name);
      });

      this.sortBy = 0;
    }
  }

  sortByType() {

    this.resetIcons();

    if(this.sortBy == 0) {

      this.typeIcon = this.ascIcon;

      // Sort the files alphabetically ascending
      this.files.sort((a, b) => {
        return this.compareStrings(a.type, b.type);
      });

      this.sortBy = 1;

    } else {

      this.typeIcon = this.descIcon;

      // Sort the files alphabetically descending
      this.files.sort((a, b) => {
        return this.compareStrings(b.type, a.type);
      });

      this.sortBy = 0;
    }
  }

  sortByDate() {

    this.resetIcons();

    if(this.sortBy == 0) {

      this.addedIcon = this.ascIcon;

      // Sort the files chronologically ascending
      this.files.sort((a, b) => {
        return this.compareNumbers(a.added, b.added);
      });

      this.sortBy = 1;

    } else {

      this.addedIcon = this.descIcon;

      // Sort the files chronologically descending
      this.files.sort((a, b) => {
        return this.compareNumbers(b.added, a.added);
      });

      this.sortBy = 0;
    }
  }

  resetIcons() {
    this.nameIcon = this.minusIcon;
    this.typeIcon = this.minusIcon;
    this.addedIcon = this.minusIcon;
  }

  openFolder(fileId: number): void{

    if(this.files[fileId].type != "folder")
      return null;

    this.crumbs = this.files[fileId].name;
    this.files = this.files[fileId].files;
  }

  goBack(){
    this.files = this.fileService.getAllFiles();
    this.crumbs = "root";
  }
  
  compareStrings(a, b) : number {
    a = a.toLowerCase();
    b = b.toLowerCase();

    return (a < b) ? -1 : (a > b) ? 1 : 0;
  }

  compareNumbers(a,b) : number {
    a = new Date(a).getTime();
    b = new Date(b).getTime();

    return (a < b) ? -1 : (a > b) ? 1 : 0;
  }

}
